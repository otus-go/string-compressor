package main

import (
	"fmt"
	"strings"
	"unicode"
)

// Compress - simple string compression
func Compress(str string) string {
	var (
		currentChar rune
	 	counter int
		builder strings.Builder
	)
	for _, char := range str {
		if char == currentChar {
			counter++
			continue
		}
		writeCompress(&builder, currentChar, counter)
		currentChar, counter = char, 1
	}
	// write last symbol
	writeCompress(&builder, currentChar, counter)
	return builder.String()
}

func writeCompress(b *strings.Builder, char rune, count int) {
	var format string
	if char == 0 {
		return
	}
	if unicode.IsDigit(char) {
		format = "\\%c"
	} else {
		format = "%c"
	}
	if count > 1 {
		fmt.Fprintf(b, format+"%d", char, count)
		return
	}
	fmt.Fprintf(b, format, char)
}

// Decompress return full string
func Decompress(str string) (string, error) {
	var (
		b           strings.Builder
		currentChar rune
		counter     = 0
		escaped     = false
	)

	for _, r := range str {

		if !escaped {
			if r == '\\' {
				// flush result if exists and set `escaped` flag
				writeDecompress(&b, currentChar, counter)
				currentChar, counter, escaped = 0, 0, true
				continue
			}
			if unicode.IsDigit(r) && currentChar == 0 {
				// return error
				return "", fmt.Errorf("Unescaped number")
			}
		}

		if unicode.IsDigit(r) && currentChar == 0 {
			currentChar, counter, escaped = r, 0, false
			continue
		}

		if unicode.IsDigit(r) {
			counter = counter*10 + int(r-'0')
			continue	
		}

		writeDecompress(&b, currentChar, counter)
		currentChar, counter, escaped = r, 0, false
	}
	writeDecompress(&b, currentChar, counter)
	return b.String(), nil
}

func writeDecompress(b *strings.Builder, r rune, n int) {
	if r == 0 {
		return
	}
	if n == 0 {
		n = 1
	}
	for i := 0; i < n; i++ {
		b.WriteRune(r)
	}
}
