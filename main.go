package main

import (
	"fmt"
	"flag"
	"os"
)

const (
	defaultDecompress = false
)

var decompress bool

func main() {
	flag.BoolVar(&decompress, "d", defaultDecompress, "set true for decompress")
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Println("Please, pass the string for processing")
		os.Exit(1)
	}
	input := flag.Arg(0)
	if decompress {
		if res, err := Decompress(input); err == nil {
			fmt.Println(res)
			return
		}
		fmt.Println("Unable to decompress")
		os.Exit(1)
	}
	fmt.Println(Compress(input))
}