package main

import (
	"testing"
)

func TestCompress(t *testing.T) {
	for _, testCase := range []struct {
		Case     string
		Expected string
	}{
		{
			"",
			"",
		},
		{
			"qwerty",
			"qwerty",
		},
		{
			"aaaabbcddd",
			"a4b2cd3",
		},
		{
			"aaaabbcddde",
			"a4b2cd3e",
		},
		{
			"кириллица",
			"кирил2ица",
		},
		{
			"aaaaa0000",
			"a5\\04",
		},
	} {
		if result := Compress(testCase.Case); result != testCase.Expected {
			t.Errorf("Got %s, expected %s", result, testCase.Expected)
		}
	}
}

func TestDecompress(t *testing.T) {
	for _, testCase := range []struct {
		Case     string
		Expected string
	}{
		{
			"",
			"",
		},
		{
			"a",
			"a",
		},
		{
			"qwerty",
			"qwerty",
		},
		{
			"a4b2cd3",
			"aaaabbcddd",
		},
		{
			"a4b2cd3e",
			"aaaabbcddde",
		},
		{
			"кирил2ица",
			"кириллица",
		},
		{
			"a5\\04",
			"aaaaa0000",
		},
		{
			"\\55",
			"55555",
		},
		{
			`qwe\\5`,
			`qwe\\\\\`,
		},
		{
			`qwe\4\5`,
			`qwe45`,
		},
		{
			"55",
			"",
		},
	} {
		if result, _ := Decompress(testCase.Case); result != testCase.Expected {
			t.Errorf("Got %s, expected %s", result, testCase.Expected)
		}
	}
}
